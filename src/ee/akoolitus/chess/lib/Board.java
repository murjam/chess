package ee.akoolitus.chess.lib;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Map.Entry;

import javax.swing.JPanel;

import ee.akoolitus.chess.piece.Piece;

@SuppressWarnings("serial")
public class Board extends JPanel {
	
	public static final int SQUARE_SIZE = 50;
	private static final int BOARD_OFFSET = 30;
	
	private Position position = new Position();
	private Piece draggedPiece;
	private Point draggedPosition;
	private Square dragStart;
	
	private double evaluation = 0;
	
	private BufferedImage buffer = new BufferedImage(getPreferredSize().width,
			getPreferredSize().height, BufferedImage.TYPE_INT_RGB);
	
	public Board() {
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				Square square = squareByEvent(e);
				draggedPiece = position.getPiece(square);
				if (draggedPiece != null) {
					dragStart = square;
				}
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				if (null != draggedPiece) {
					if (draggedPiece.isDark() == position.isDarkTurn()) {
						Square square = squareByEvent(e);
						if (square != null) {
							List<Square> moves = draggedPiece.getLegalMoves(dragStart, position.getPosition());
							
							// TODO: castling
							// TODO: check
							// TODO: checkmate
							// TODO: stalemate
							// TODO: en passant
							// TODO: promotion
							
							if (moves.contains(square)) {
								position.turn(draggedPiece, dragStart, square);
								evaluation = Evaluator.getEvaluation(position);
							}
							else {
								System.out.println(
										"Not allowed to move to " + square);
							}
						}
					}
					else {
						System.out.println(position.isDarkTurn() ? "Black's turn!" : "White's turn!");
					}
					draggedPiece = null;
					
					// TODO: check if it is possible to move
					// from dragStart to square
				}
				repaint();
			}
			
		});
		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (null != draggedPiece) {
//					System.out.println("Dragging " + draggedPiece.getClass().getSimpleName()
//							+ " from " + dragStart);
					draggedPosition = e.getPoint();
					repaint();
				}
			}
		});
	}
	
	private Square squareByEvent(MouseEvent e) {
		int rank = (8 - (e.getY() -  + BOARD_OFFSET) / Board.SQUARE_SIZE);
		int file = (e.getX() - BOARD_OFFSET) / Board.SQUARE_SIZE + 1;
		
		try {
			return new Square(File.findFileByPosition(file), rank);
		}
		catch (IllegalArgumentException ex) {
			return null;
		}
	}
	
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(SQUARE_SIZE * 8 + BOARD_OFFSET * 2, SQUARE_SIZE * 8 + BOARD_OFFSET * 2);
	}
	@Override
	public void paintComponent(Graphics g1) {
		Graphics2D global = (Graphics2D) g1;
		Graphics2D g = buffer.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getPreferredSize().width, getPreferredSize().height);
		
		
		g.setColor(Color.white);
		for (File file : File.values()) {
			int x = BOARD_OFFSET + SQUARE_SIZE * file.getPosition() - SQUARE_SIZE / 2 - 5;
			int y1 = (int)(SQUARE_SIZE * 8 + BOARD_OFFSET * 1.5);
			int y2 = (int)(BOARD_OFFSET * .5 + 5);
			g.drawString(file.name(), x, y1);
			g.drawString(file.name(), x, y2);
		}
		for (int rank = 1; rank <= 8; rank++) {
			int x1 = (int) (BOARD_OFFSET * .5) - 5;
			int x2 = (int) (SQUARE_SIZE * 8 + BOARD_OFFSET * 1.5) - 3;
			int y = (int)(SQUARE_SIZE * (8 - rank) + BOARD_OFFSET + SQUARE_SIZE / 2 + 7);
			g.drawString(String.valueOf(rank), x1, y);
			g.drawString(String.valueOf(rank), x2, y);
		}
		for (Square square : position.getPosition().keySet()) {
			g.setColor(square.isDark() ? new Color(150, 170, 255) : Color.WHITE);
			g.fillRect(
					(square.getFile().getPosition() - 1) * SQUARE_SIZE + BOARD_OFFSET,
					(8 - square.getRank()) * SQUARE_SIZE + BOARD_OFFSET,
					SQUARE_SIZE,
					SQUARE_SIZE);
		}
		for (Entry<Square, Piece> entry : position.getPosition().entrySet()) {
			Piece piece = entry.getValue();
			Square square = entry.getKey();
			if (piece != null) {
				
				if (piece == draggedPiece) {
					g.drawImage(piece.getImage(),
							draggedPosition.x - SQUARE_SIZE / 2,
							draggedPosition.y - SQUARE_SIZE / 2, null);
				}
				else {
					g.drawImage(piece.getImage(),
							(square.getFile().getPosition() - 1) * SQUARE_SIZE + BOARD_OFFSET,
							(8 - square.getRank()) * SQUARE_SIZE + BOARD_OFFSET, null);
				}
				
			}
		}
		g.setColor(Color.red);
		g.drawString(String.format("%.1f", evaluation), 10, 30);
		
		global.drawImage(buffer, 0, 0, null);
	}
	
	
	
}
