package ee.akoolitus.chess.lib;

import java.util.HashMap;
import java.util.Map;

import ee.akoolitus.chess.piece.Bishop;
import ee.akoolitus.chess.piece.King;
import ee.akoolitus.chess.piece.Knight;
import ee.akoolitus.chess.piece.Pawn;
import ee.akoolitus.chess.piece.Piece;
import ee.akoolitus.chess.piece.Queen;
import ee.akoolitus.chess.piece.Rook;

public class Position {

	// TODO add last move
	private Map<Square, Piece> position = new HashMap<>();
	private boolean darkTurn = false;
	
	public Position() {
		for (File file : File.values()) {
			for (int rank = 1; rank <= 8; rank++) {
				position.put(new Square(file, rank), null);
			}
		}
		createPieces();
	}
	
	public boolean isDarkTurn() {
		return darkTurn;
	}
	
	public void turn(Piece draggedPiece, Square fromSquare, Square toSquare) {
		position.put(fromSquare, null);
		position.put(toSquare, draggedPiece);
		darkTurn = !darkTurn;
	}
	
	public Map<Square, Piece> getPosition() {
		return position;
	}
	
	public Piece getPiece(Square square) {
		return position.get(square);
	}
	
	private void createPieces() {
		for (int i = 0; i < 2; i++) {
			boolean dark = i == 1;
			
			int rank = dark ? 8 : 1;
			
			position.put(new Square(File.A, rank), new Rook(dark));
			position.put(new Square(File.H, rank), new Rook(dark));
			position.put(new Square(File.B, rank), new Knight(dark));
			position.put(new Square(File.G, rank), new Knight(dark));
			position.put(new Square(File.C, rank), new Bishop(dark));
			position.put(new Square(File.F, rank), new Bishop(dark));
			position.put(new Square(File.D, rank), new Queen(dark));
			position.put(new Square(File.E, rank), new King(dark));
		
			for (File file : File.values()) {
				position.put(new Square(file, dark ? 7 : 2), new Pawn(dark));
			}
		}
	}
}
