package ee.akoolitus.chess.lib;

public class Square {

	private File file;
	private int rank;
	
	public Square(int file, int rank) {
		this(File.findFileByPosition(file), rank);
	}
	public Square(File file, int rank) {
		super();
		this.file = file;
		int position = file.getPosition();
		
		if (position > 8 || position < 1) {
			throw new IllegalArgumentException("File position cannot be "
					+ position + ". It has to be 1..8");
		}
		
		if (rank > 8 || rank < 1) {
			throw new IllegalArgumentException("Rank cannot be " + rank + ". "
					+ "It has to be 1..8");
		}
		this.rank = rank;
	}
	

	public File getFile() {
		return file;
	}
	public int getRank() {
		return rank;
	}
	
	@Override
	public int hashCode() {
		return file.getPosition() * 10 + rank;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof Square) {
			Square other = (Square) obj;
			return other.rank == rank && other.file.equals(file);
		}
		
		return super.equals(obj);
	}

	public boolean isDark() {
		return (file.getPosition() + rank) % 2 == 0;
	}
	
	@Override
	public String toString() {
		return String.format("%s%d", file.name(), rank);
	}
	
	public Square getNeighbour(Direction direction) {
		switch (direction) {
		case DOWN:
			return getSquare(0, -1);
		case LEFT:
			return getSquare(-1, 0);
		case RIGHT:
			return getSquare(1, 0);
		case UP:
			return getSquare(0, 1);
		case UP_LEFT:
			return getSquare(-1, 1);
		case UP_RIGHT:
			return getSquare(1, 1);
		case DOWN_RIGHT:
			return getSquare(1, -1);
		case DOWN_LEFT:
			return getSquare(-1, -1);
		default:
			throw new IllegalStateException("No such direction!");
		}
	}
	
	public Square getSquare(int fileDiff, int rankDiff) {
		try {
			return new Square(this.getFile().getPosition() + fileDiff,
					this.getRank() + rankDiff);
		}
		catch (IllegalArgumentException e) {
			return null;
		}
	}
}
