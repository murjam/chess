package ee.akoolitus.chess.lib;

public enum Direction {
	UP, DOWN, LEFT, RIGHT,
	UP_LEFT, UP_RIGHT, DOWN_RIGHT, DOWN_LEFT;
	
	public static Direction[] straightDirections() {
		return new Direction[]{
			UP, DOWN, LEFT, RIGHT
		};
	}
	
	public static Direction[] diagonalDirections() {
		return new Direction[]{
			UP_LEFT, UP_RIGHT, DOWN_RIGHT, DOWN_LEFT
		};
	}
}
