package ee.akoolitus.chess.lib;

import java.util.Map.Entry;
import java.util.Set;

import ee.akoolitus.chess.piece.Bishop;
import ee.akoolitus.chess.piece.King;
import ee.akoolitus.chess.piece.Knight;
import ee.akoolitus.chess.piece.Pawn;
import ee.akoolitus.chess.piece.Piece;
import ee.akoolitus.chess.piece.Queen;
import ee.akoolitus.chess.piece.Rook;

public class Evaluator {

	public static double getEvaluation(Position position) {
		double evaluation = 0;

		Set<Entry<Square,Piece>> entries = position.getPosition().entrySet();
		for (Entry<Square, Piece> entry : entries) {
			Piece piece = entry.getValue();
			if (piece == null) {
				continue;
			}
			Square square = entry.getKey();
			int rankValue = piece.isDark() ? 9 - square.getRank() : square.getRank();
			
			double pieceValue = 1;
			
			if (piece instanceof Queen) {
				pieceValue = 9;
			}
			else if (piece instanceof Rook) {
				pieceValue = 5;
			}
			else if (piece instanceof Bishop) {
				pieceValue = 3.1;
			}
			else if (piece instanceof Knight) {
				pieceValue = 3;
			}
			else if (piece instanceof King) {
				pieceValue = 2; // king's attack value
			}
			else if (piece instanceof Pawn) {
				pieceValue = 1 + rankValue / 6.;
			}
			int possibleMoveCount = piece.getLegalMoves(square, position.getPosition()).size();
			pieceValue += possibleMoveCount / 10.;
			
			evaluation += piece.isDark() ? -pieceValue : pieceValue;
		}
		
		return evaluation;
	}
	
}
