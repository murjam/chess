package ee.akoolitus.chess.lib;

public enum File {

	A(1),
	B(2),
	C(3),
	D(4),
	E(5),
	F(6),
	G(7),
	H(8);
	
	private int position;
	
	private File(int position) {
		this.position = position;
	}
	
	public int getPosition() {
		return position;
	}
	
	public static File findFileByPosition(int position) {
		File[] values = File.values();
		for (File file : values) {
			if (file.getPosition() == position) {
				return file;
			}
		}
		throw new IllegalArgumentException("No file to match number " + position);
	}
}
