package ee.akoolitus.chess.piece;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ee.akoolitus.chess.lib.Direction;
import ee.akoolitus.chess.lib.Square;

public class King extends Piece {

	public King(boolean dark) {
		super(dark);
	}
	
	@Override
	protected void drawHead(Graphics2D g) {
		
		int c = (int) getCenter();
		
		g.fillOval(c - 10, 12, 10, 10);
		g.fillOval(c - 5, 7, 10, 10);
		g.fillOval(c, 12, 10, 10);
		
		g.fillRect(c - 6, 15, 12, 20);
	}
	
	@Override
	public List<Square> getLegalMoves(Square start, Map<Square, Piece> position) {
		List<Square> moves = new ArrayList<>();
		
		for (Direction direction : Direction.values()) {
			Square square = start.getNeighbour(direction);
			if (square != null) {
				Piece p = position.get(square);
				if (noPiece(p) || isOpponent(p)) {
					moves.add(square);
				}
			}
		}
		
		
		return moves;
	}
	
}
