package ee.akoolitus.chess.piece;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ee.akoolitus.chess.lib.Square;

public class Knight extends Piece {

	public Knight(boolean dark) {
		super(dark);
	}
	
	@Override
	public List<Square> getLegalMoves(Square start, Map<Square, Piece> position) {
		List<Square> moves = new ArrayList<>();
		
		int[][] combinations = {
			{-2, -1},
			{-1, -2},
			{2, 1},
			{1, 2},
			{-2, 1},
			{1, -2},
			{2, -1},
			{-1, 2}
		};
		
		for (int[] xy : combinations) {
			Square square = start.getSquare(xy[0], xy[1]);
			if (square != null) {
				Piece p = position.get(square);
				if (noPiece(p) || isOpponent(p)) {
					moves.add(square);
				}
			}
		}
		
		return moves;
	}

}
