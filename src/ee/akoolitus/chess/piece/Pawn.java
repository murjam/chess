package ee.akoolitus.chess.piece;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ee.akoolitus.chess.lib.Square;

public class Pawn extends Piece {

	public Pawn(boolean dark) {
		super(dark);
	}

	@Override
	protected void drawHead(Graphics2D g) {
		int radius = 10;
		int center = (int) getCenter();
		g.fillOval(center - radius, center - radius - 6, radius *  2, radius * 2);
	}
	
	@Override
	public List<Square> getLegalMoves(Square start, Map<Square, Piece> position) {
		ArrayList<Square> moves = new ArrayList<>();
		
		Square step = start.getSquare(0, isDark() ? -1 : 1);
		if (noPiece(position.get(step))) {
			moves.add(step);
			
			int rank = start.getRank();
			if (rank == (isDark() ? 7 : 2)) {
				Square twoStep = step.getSquare(0, isDark() ? -1 : 1);
				if (noPiece(position.get(twoStep))) {
					moves.add(twoStep);
				}
			}
		}
		
		Square takeRight = start.getSquare(isDark() ? -1 : 1, isDark() ? -1 : 1);
		if (isOpponent(position.get(takeRight))) {
			moves.add(takeRight);
		}
		Square takeLeft = start.getSquare(isDark() ? 1 : -1, isDark() ? -1 : 1);
		if (isOpponent(position.get(takeLeft))) {
			moves.add(takeLeft);
		}
		return moves;
	}

}
