package ee.akoolitus.chess.piece;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import ee.akoolitus.chess.lib.Board;
import ee.akoolitus.chess.lib.Square;


public abstract class Piece {

	private boolean isDark;
	
	public Piece(boolean dark) {
		isDark = dark;
	}
	
	public boolean isDark() {
		return isDark;
	}

	public Image getImage() {
		BufferedImage image = new BufferedImage(Board.SQUARE_SIZE, Board.SQUARE_SIZE, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = image.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(isDark ? Color.BLACK : Color.ORANGE);
		drawFoot(g);
		drawHead(g);
		return image;
	}
	
	protected double getCenter() {
		return Board.SQUARE_SIZE / 2.; 
	}
	
	protected double getSize() {
		return Board.SQUARE_SIZE / 1.4;
	}
	
	protected void drawFoot(Graphics2D g) {
		double center = getCenter();
		double size = getSize();
		
		Area area = new Area(new Ellipse2D.Double(center - size / 2, center - 2, size, size));
		
		int cutoff = Board.SQUARE_SIZE / 7;
		area.subtract(new Area(new Rectangle2D.Double(
				0,
				Board.SQUARE_SIZE - cutoff,
				Board.SQUARE_SIZE,
				cutoff)));
		
		g.fill(area);
	}
	
	protected void drawHead(Graphics2D g) {
		g.drawString(this.getClass().getSimpleName(),
				3, (int) (getSize() / 2));
	}
	
	public List<Square> getLegalMoves(Square start, Map<Square, Piece> position) {
		return Collections.emptyList();
	}
	
	public boolean isPiece(Piece p) {
		return p != null;
	}
	
	public boolean noPiece(Piece p) {
		return p == null;
	}
	
	public boolean isOpponent(Piece p) {
		return isPiece(p) && p.isDark() != this.isDark();
	}
	
	public boolean isOwn(Piece p) {
		return isPiece(p) && p.isDark() == this.isDark();
	}
	
}
