package ee.akoolitus.chess.piece;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ee.akoolitus.chess.lib.Direction;
import ee.akoolitus.chess.lib.Square;

public class Queen extends Piece {

	public Queen(boolean dark) {
		super(dark);
	}

	
	@Override
	protected void drawHead(Graphics2D g) {
		int c = (int) getCenter();
		
		Polygon p = new Polygon();
		
		p.addPoint(c + 10, c + 5);
		p.addPoint(c - 10, c + 5);
		
		p.addPoint(c - 20, 10); //
		g.fillOval(c - 23, 7, 6, 6);
		
		p.addPoint(c - 7, 20);
		p.addPoint(c - 10, 5); //
		g.fillOval(c - 13, 2, 6, 6);
		
		p.addPoint(c, 18);
		
		p.addPoint(c + 10, 5); //
		g.fillOval(c + 7, 2, 6, 6);
		
		p.addPoint(c + 7, 20);
		p.addPoint(c + 20, 10); //
		g.fillOval(c + 17, 7, 6, 6);
		
		g.fillPolygon(p);
	}
	
	@Override
	public List<Square> getLegalMoves(Square start, Map<Square, Piece> position) {
		List<Square> moves = new ArrayList<>();
		
		for (Direction direction : Direction.values()) {
			Square square = start;
			for (int i = 0; i < 7; i++) {
				square = square.getNeighbour(direction);
				if (square == null) {
					break;
				}
				Piece p = position.get(square);
				if (noPiece(p)) {
					moves.add(square);
					continue;
				}
				if (isOpponent(p)) {
					moves.add(square);
					break;
				}
				break;
			}
		}
		
		
		return moves;
	}
}
