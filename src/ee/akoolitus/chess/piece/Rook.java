package ee.akoolitus.chess.piece;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ee.akoolitus.chess.lib.Direction;
import ee.akoolitus.chess.lib.Square;

public class Rook extends Piece {

	public Rook(boolean dark) {
		super(dark);
	}

	@Override
	protected void drawHead(Graphics2D g) {
		int center = (int) getCenter();
		int size = (int) (getSize() / 2);
		int width = size - 4;
		
		Polygon p = new Polygon();
		p.addPoint(center - width, center + size);
		p.addPoint(center - width, center - size - 2);
		p.addPoint(center - width + 6, center - size - 2);
		p.addPoint(center - width + 6, center - size);
		p.addPoint(center - 3, center - size);
		p.addPoint(center - 3, center - size - 2);
		p.addPoint(center + 3, center - size - 2);
		p.addPoint(center + 3, center - size);
		p.addPoint(center + width - 6, center - size);
		p.addPoint(center + width - 6, center - size - 2);
		p.addPoint(center + width, center - size - 2);
		p.addPoint(center + width, center + size);
		
		g.fill(new Area(p));
	}
	
	@Override
	public List<Square> getLegalMoves(Square start, Map<Square, Piece> position) {
		List<Square> moves = new ArrayList<>();
		
		for (Direction direction : Direction.straightDirections()) {
			Square square = start;
			for (int i = 0; i < 7; i++) {
				square = square.getNeighbour(direction);
				if (square == null) {
					break;
				}
				Piece p = position.get(square);
				if (noPiece(p)) {
					moves.add(square);
					continue;
				}
				if (isOpponent(p)) {
					moves.add(square);
					break;
				}
				break;
			}
		}
		
		return moves;
	}
}
