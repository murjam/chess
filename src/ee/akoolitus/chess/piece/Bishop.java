package ee.akoolitus.chess.piece;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ee.akoolitus.chess.lib.Direction;
import ee.akoolitus.chess.lib.Square;

public class Bishop extends Piece {

	public Bishop(boolean dark) {
		super(dark);
	}
	
	@Override
	protected void drawHead(Graphics2D g) {
		int c = (int) getCenter();
		
		Polygon p = new Polygon();
		
		p.addPoint(c, 5);
		p.addPoint(c + 14, c + 10);
		p.addPoint(c - 14, c + 10);
		
		g.fillPolygon(p);
	}
	
	@Override
	public List<Square> getLegalMoves(Square start, Map<Square, Piece> position) {
		List<Square> moves = new ArrayList<>();
		
		for (Direction direction : Direction.diagonalDirections()) {
			Square square = start;
			for (int i = 0; i < 7; i++) {
				square = square.getNeighbour(direction);
				if (square == null) {
					break;
				}
				Piece p = position.get(square);
				if (noPiece(p)) {
					moves.add(square);
					continue;
				}
				if (isOpponent(p)) {
					moves.add(square);
					break;
				}
				break;
			}
		}
		
		return moves;
	}

}
