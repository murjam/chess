package ee.akoolitus.chess;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import ee.akoolitus.chess.lib.Board;

public class ChessStart implements Runnable {
	Board board = new Board();
	
	@Override
	public void run() {
		final JFrame window = new JFrame("Chess!");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);
		window.setContentPane(board);
		window.pack();
		window.setVisible(true);
		
		window.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (KeyEvent.VK_ESCAPE == e.getKeyCode()) {
					System.exit(0);
				}
			}
		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new ChessStart());
	}
}
